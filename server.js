var http = require('http'),
  express = require('express'),
  router = express(),
  shot=require('./shot'),
  imgDir=process.env.PWD+'/img/';

var server = http.createServer(router);
router.get('/',function(req,res){res.send('Руководство:\n/shot?url=URL - снимок страницы');});
router.get('/shot',function(req,res){
  var url=req.query.url;
    console.log('[REQUEST]',req.query);
  if(!url)
    return res.status(400).send('Url not defined!');
  shot.shot(url,function(path){
    res.sendFile(path,{root:process.env.PWD});
  });
});
router.get('/clear',function(req,res){
  var url=req.query.url || '',
    all=req.query.all || false,
    type= all && 'all' || url && 'url' || 'old';
  shot.clear(type,url,function(){
    res.send('Done');
  });
});

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("PageShoter is working now at", addr.address + ":" + addr.port);
});