var parse=require('url').parse,
    fs=require('fs'),
    cp=require('child_process').exec,
    dir='img/',
    storePeriod=24*60*60*1000;//снимок устаревает за день
module.exports={
    clear:clear,
    shot:shot,
    isOld:isOld
};
function url2path(url){
    var parsed=parse(url);
    return (parsed.hostname+parsed.pathname).replace(/[\/.]/g,'')+'.jpeg';
}
function isOld(path){
    try{
        var data=fs.statSync(path),
            dtime=new Date() - new Date(data.ctime);
        return dtime>storePeriod;
    }catch(e){
        return true;
    }
}
function runcmd(cmd,cb){
    console.log('[CMD]\t',cmd);
    cp(cmd,function(a,b,c){
        console.log('[DONE]\t',cmd);
        cb();
    });
}
function shot(url,cb){
    var path=dir+url2path(url),
        cmdShot="xvfb-run wkhtmltoimage --format jpeg --quality 40 --disable-smart-width --width 800 --height 1200 --no-debug-javascript "+url+" "+path,
        cmdResize="convert "+path+" -resize 800x600 "+path;
    if(!isOld(path))
        return cb(path);
    runcmd(cmdShot,function(){
        runcmd(cmdResize,function(a, b, c) {
            cb(path);
        });
    });
}
function clear(type,url,cb){
    fs.readdir(dir,function(err,files){
        if(err){
            console.log('[ERROR]',err);
            return cb();
        }
        if(type=='url'){
            files=[url2path(url)];
            console.log('[CLEAR] url:',files);
        }else if(type=='all'){
            console.log('[CLEAR] all:',files);
        }else if(type=='old'){
            var files=files.filter(function(path){
                return isOld(dir+path);
            });
            console.log('[CLEAR] old:',files);
        }
        files.forEach(function(path){
            fs.unlinkSync(dir+path);
        });
        return cb();
    });
}